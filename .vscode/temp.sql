SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE DATABASE IF NOT EXISTS `comp6002_17a_assn2` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `comp6002_17a_assn2`;

DROP TABLE IF EXISTS `tbl_admin`;
CREATE TABLE `tbl_admin` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(20) NOT NULL,
  `PASSWRD` varchar(20) NOT NULL,
  `CATEGORY` varchar(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_admin` (`ID`, `NAME`, `PASSWRD`, `CATEGORY`) VALUES
(1, 'admin', 'admin123', '');

DROP TABLE IF EXISTS `tbl_consumables`;
CREATE TABLE `tbl_consumables` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) NOT NULL,
  `PRICE` double NOT NULL,
  `CATEGORY` varchar(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_consumables` (`ID`, `NAME`, `PRICE`, `CATEGORY`) VALUES
(2, 'Latte', 2.75, 'Drinks'),
(3, 'Chocolate Cake', 3.5, 'SIDES'),
(4, 'Hot Chips', 3.00, 'SIDES'),
(6, 'Something', 3.00, 'Drinks');