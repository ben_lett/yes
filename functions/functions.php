<?php 

include_once('creds.php');

////////////////////////////////////////////////////////////////
/////////              MySQLi Functions           //////////////
////////////////////////////////////////////////////////////////

function connection() {
    
    $conn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
    
    if ($conn->connect_errno > 0) {
        die('Unable to connect to database ['.$conn->connect_errno.']');
    }
    
    return $conn;
}

////////////////////////////////////////////////////////////////
/////////              Show all Data              //////////////
////////////////////////////////////////////////////////////////

function get_all_items($category) {
    
    $db = connection();
    $sql = "SELECT * FROM tbl_consumables WHERE category = '$category' ";
    $arr = [];
    
    $result = $db->query($sql);
    
    if(!$result) {
        die("There was an error running the query [".$db->error."] ");
    }
    
    while ($row = $result->fetch_assoc()) {
        $arr[] = array (
            "id" => $row['ID'],
            "name" => $row['NAME'],
            "price" => $row['PRICE'],
            "category" => $row['CATEGORY']
        );
    }
    
    $json = json_encode($arr);
    
    $result->free();
    $db->close();
    
    return $json;
}

function show_all_items($data, $page) {
    
    $array = json_decode($data, True);

    
    $output = "";

    if (count($array) > 0 ) {
        for ($i = 0; $i < count($array); $i++) {
            
            
            if ($page == "index") {
                //String for HTML table code
            $output .= "<tr><td>".$array[$i]['name']."</td><td>".$array[$i]['price']."</td></tr>";
            }
            
            if ($page == "admin") {
                //String for HTML table code
                $output .= "<tr><td>".$array[$i]['name']."</td><td>".$array[$i]['price']."</td><td><a href=\"edit.php?id=".$array[$i]['id']."\">Edit</a></td><td><a href=\"delete.php?id=".$array[$i]['id']."\">Delete</a></td></tr>";    
            }   
        }
        
        return $output;
    }
    else {
        $output .= "<tr><td colspan='5'>No Data Available</td></tr>";
        
        return $output;
    }
}
// EDIT FUNCTION

function editRecord() {

    if(isset($_POST['updateItem']))
    {
        $db = connection();

        $name = $db->real_escape_string($_POST['name']);
        $price = $db->real_escape_string($_POST['price']);
        $category = $db->real_escape_string($_POST['category']);
        $id = $db->real_escape_string($_POST['id']);

        $sql = "UPDATE tbl_consumables SET NAME='".$name."', PRICE='".$price."', CATEGORY='".$category."' WHERE ID = ".$id."";

        $result = $db->query($sql);

        if ($result == 1) {
            redirect("index.php");
        }
        else {
            print_r($sql);
            return "<br><br>An Error has occured";
            exit();
        }
    }  
}

//ADD FUNCTION

function addRecord() {
    if(isset($_POST['addItem']))
    {
        $db = connection();

        $name = $db->real_escape_string($_POST['name']);
        $price = $db->real_escape_string($_POST['price']);
        $category = $db->real_escape_string($_POST['category']);

        $stmt = $db->prepare("INSERT INTO tbl_consumables (NAME, PRICE, CATEGORY) VALUES (?,?,?)");
        $stmt->bind_param("sss", $name, $price, $category);
        $stmt->execute();
        
        print $stmt->error; //to check errors

        $result = $stmt->affected_rows;

        $stmt->close();
        $db->close();

        if ($result > 0) {
            redirect("index.php");
        }
        else {
            print_r($sql);
            echo "<br><br>";
            echo "An Error has occured";
        }
    }
}

// DELETE FUNCTION

function removeSingleRecord() {

    if(isset($_POST['removeRecord']))
    {
        $db = connection();

        $id = $db->real_escape_string($_POST['id']);

        $stmt = $db->prepare("DELETE FROM tbl_consumables WHERE ID = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        
        print $stmt->error; //to check errors

        $result = $stmt->affected_rows;

        $stmt->close();
        $db->close();

        if ($result == 1) {
            redirect("index.php");
        }
        else {
            print_r($sql);
            echo "<br><br>";
            echo "An Error has occured";
        }
    }
}

// DISPLAY NAMES
function displayId()
{
    $id = $_GET['id'];
    $array = json_decode(loadData($id), True);
    return $array[0]['id'];
}
function displayName()
{
    $id = $_GET['id'];
    $array = json_decode(loadData($id), True);
    return $array[0]['name'];
}

function displayPrice()
{
    $id = $_GET['id'];
    $array = json_decode(loadData($id), True);
    return $array[0]['price'];
}
// LOAD DATA
function loadData($id) {

    $db = connection();
    $sql = "SELECT * FROM tbl_consumables WHERE ID = $id";
    $arr = [];
    
    $result = $db->query($sql);
    
    if(!$result) {
        die("There was an error running the query [".$db->error."] ");
    }
    
    while ($row = $result->fetch_assoc()) {
        $arr[] = array (
            "id" => $row['ID'],
            "name" => $row['NAME'],
            "price" => $row['PRICE'],
            "category" => $row['CATEGORY']
        );
    }
    
    $json = json_encode($arr);
    
    $result->free();
    $db->close();
    
    return $json;        
}
// LOGIN/LOGOUT ADMIN 

function loginAdmin() {

    if(isset($_POST['login']))
    {
        $db = connection();

        $user = $db->real_escape_string($_POST['name']);
        $pass = $db->real_escape_string($_POST['passwrd']);

        $sql = "SELECT * FROM tbl_admin WHERE NAME = '$user' && PASSWRD = '$pass'";
        $arr = [];

        $result = $db->query($sql);

        if(!$result) {
            die("There was an error running the query [".$db->error."] ");
        }
        
        while ($row = $result->fetch_assoc()) {
            $arr[] = array (
                "user" => $row['NAME'],
                "pass" => $row['PASSWRD']
            );
        }

        $result->free();
        $db->close();

        if (($user == $arr[0]['user']) && ($pass == $arr[0]['pass']))
        {
            $_SESSION['login'] = TRUE;
            redirect("admin/index.php");
        }
        else
        {
            $_SESSION['login'] = FALSE;
            echo "<h1 class='removeSure'>Your login details are incorrect.</h1>";
        }
    }
}

function logout()
{
    if(isset($_POST['logout']))
    {
        session_start();

        // Unset all of the session variables.
        $_SESSION = array();

        // If it's desired to kill the session, also delete the session cookie.
        // Note: This will destroy the session, and not just the session data!

        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }

        // Finally, destroy the session.
        session_destroy();

        redirect("../index.php");
    }
}

//  NON MySQLi Functions

function redirect($location)
{
    $URL = $location;
    echo "<script type='text/javascript'>document.location.href='{$URL}';</script>";
    echo '<META HTTP-EQUIV="refresh" content="0;URL=' . $URL . '">';
    exit();
}
